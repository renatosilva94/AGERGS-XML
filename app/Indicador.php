<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicador extends Model {

    public $timestamps = false;
    
    protected $fillable = array('ano_mes','mes_ano','viagens_realizadas', 'viagensrealizadasPrecisao', 'viagens_programadas','viagensprogramadasPrecisao' , 'viagens_impontuais', 'viagensimpontuaisPrecisao', 'viagens_interrompidas','viagensinterrompidasPrecisao' ,'ocorrencia_quebra', 'desvio_itinerario', 'viagens_abaixo_80',
        'viagens_acima_80', 'viagens_100', 'acidentes_transito', 'icpo', 'icpoPrecisao', 'iphs', 'iphsPrecisao', 'icvi', 'icviPrecisao', 'imfu', 'imfuPrecisao', 'ioq', 'ioqPrecisao', 'idi', 'idiPrecisao',
        'io1', 'io1Precisao', 'io2', 'io2Precisao', 'io3', 'io3Precisao', 'ioap', 'ioapPrecisao');

}
