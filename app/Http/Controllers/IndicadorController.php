<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Indicador;


class IndicadorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
       $indicadores = Indicador::all();

        return view('indicador_show', compact('indicadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $acao = 1;

        //$empresas = Empresa::all();
        //$linhas = Linha::all();

        
        return view('indicador_form', compact('acao')); // 'linhas', 'empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $dados = $request->all();

        $indicadores = Indicador::create($dados);

        //se inclusão ok

        if ($indicadores) {
            return redirect()->route('indicadores.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $indicadores = new Indicador();



        $reg = $indicadores->find($id);


        $acao = 2;

        return view('indicador_form', compact('reg', 'acao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $reg = Indicador::find($id);

        $dados = $request->all();

        $alterado = $reg->update($dados);

        if ($alterado) {
            return redirect()->route('indicadores.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $reg = Indicador::find($id);

        $reg->delete();
//
        return redirect()->route('indicadores.index');
    }

}
