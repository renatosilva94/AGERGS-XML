<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
    public $timestamps = false;
    
    protected $fillable = array('cod_linha', 'nome_linha', 'modalidade');
}
