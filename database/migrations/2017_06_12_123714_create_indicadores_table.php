<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicadoresTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('indicadors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ano_mes');

            $table->string('mes_ano');


            $table->integer('viagens_realizadas');
            $table->char('viagensrealizadasPrecisao');

            $table->integer('viagens_programadas');
            $table->char('viagensprogramadasPrecisao');


            $table->integer('viagens_impontuais');
            $table->char('viagensimpontuaisPrecisao');



            $table->integer('viagens_interrompidas');
            $table->char('viagensinterrompidasPrecisao');


            $table->integer('ocorrencia_quebra');
            $table->integer('desvio_itinerario');
            $table->integer('viagens_abaixo_80');
            $table->integer('viagens_acima_80');
            $table->integer('viagens_100');
            $table->integer('acidentes_transito');
            $table->double('icpo');
            $table->char('icpoPrecisao');
            $table->double('iphs');
            $table->char('iphsPrecisao');
            $table->double('icvi');
            $table->char('icviPrecisao');
            $table->double('imfu');
            $table->char('imfuPrecisao');
            $table->double('ioq');
            $table->char('ioqPrecisao');
            $table->double('idi');
            $table->char('idiPrecisao');
            $table->double('io1');
            $table->char('io1Precisao');
            $table->double('io2');
            $table->char('io2Precisao');
            $table->double('io3');
            $table->char('io3Precisao');
            $table->double('ioap');
            $table->char('ioapPrecisao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('indicadors');
    }

}
