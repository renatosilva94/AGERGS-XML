<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('linhas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_linha', 50);
            $table->string('nome_linha', 100);
            $table->enum('modalidade', ['CM', 'DI', 'SD', 'IO', 'RT', 'IT', 'ST', 'EX']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('linhas');
    }

}
