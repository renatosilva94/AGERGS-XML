<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('empresas')->insert([
        'cnpj' => '93841161000199',
        'razao_social' => 'EMPRESA DE TRANSPORTES BOSEMBECKER LTDA'
       ]);

}

}
