<?php

use Illuminate\Database\Seeder;

class LinhasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('linhas')->insert([
        'cod_linha' => 'P100',
        'nome_linha' => 'Capão do Leão',
        'modalidade' => 'CM'
       ]);

    }
}
