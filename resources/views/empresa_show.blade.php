@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2>Cadastro de Empresas</h2>
</div>

<div class='col-sm-1'>
    <a href="{{route('empresas.create')}}" class="btn btn-danger" role="button">Nova Empresa</a>
</div>

<div class='col-sm-12'>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Cód</th>
                <th>CNPJ</th>
                <th>Razão_Social</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>

            @foreach($empresas as $empresa)

            <tr>
                <td>{{$empresa->id}}</td>
                <td>{{$empresa->cnpj}}</td>
                <td>{{$empresa->razao_social}}</td>
                
                <td>
                    <a href="{{route('empresas.edit', $empresa->id)}}" class="btn btn-success" role="button">Editar</a>
                    <form style="display: inline-block;"
                          method="POST"
                          action="{{route('empresas.destroy', $empresa->id)}}"
                          onsubmit="return confirm('Confirmar Exclusão ?')">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger">Excluir</button>
                    </form>

                </td>

            </tr>

            @endforeach
        </tbody>
    </table>
</div>



@endsection

