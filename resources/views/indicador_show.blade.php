@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2>Cadastro de Indicadores</h2>
</div>

<div class="col-sm-6">



    <div class='col-sm-3'>
        <a href="{{route('indicadores.create')}}" class="btn btn-primary" role="button">Novo Indicador</a>

    </div>

</div>




<div class='col-sm-12'>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Cód</th>
                <th>Viagens_Realizadas</th>
                <th>Viagens_Programadas</th>
                <th>Viagens_Impontuais</th>
                <th>Viagens_Interrompidas</th>
                <th>Ocorrencia_Quebra</th>
                <th>Desvio_Itinerario</th>
                <th>Viagens_Abaixo_80</th>
                <th>Viagens_Acima_80</th>
                <th>Viagens_100</th>
                <th>Acidentes_Transito</th>
                <th>Ações</th>

            </tr>
        </thead>
        <tbody>

            @foreach($indicadores as $indicador)

            <tr>
                <td>{{$indicador->id}}</td>
                <td>{{$indicador->viagens_realizadas}}</td>
                <td>{{$indicador->viagens_programadas}}</td>
                <td>{{$indicador->viagens_impontuais}}</td>
                <td>{{$indicador->viagens_interrompidas}}</td>
                <td>{{$indicador->ocorrencia_quebra}}</td>
                <td>{{$indicador->desvio_itinerario}}</td>
                <td>{{$indicador->viagens_abaixo_80}}</td>
                <td>{{$indicador->viagens_acima_80}}</td>
                <td>{{$indicador->viagens_100}}</td>
                <td>{{$indicador->acidentes_transito}}</td>












                <td>
                    <a href="{{route('indicadores.edit', $indicador->id)}}" class="btn btn-success" role="button">Editar</a>
                    <form style="display: inline-block;" method="POST" action="{{route('indicadores.destroy', $indicador->id)}}"
                          onsubmit="return confirm('Confirmar Exclusão ?')">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger">Excluir</button>


                    </form>

                    <a href="{{route('xml.index')}}" class="btn btn-primary">Gerar XML</a>

                </td>

            </tr>

            @endforeach
        </tbody>
    </table>
</div>

@endsection