@extends('principal')

@section('conteudo')

@if ($acao == 1)
    <form method="post" action="{{route('empresas.store')}}">
@else
    <form method="post" action="{{route('empresas.update', $reg->id)}}">
    {!! method_field('put') !!}
@endif
        {{ csrf_field() }}

    <div class="col-sm-4">
        <label for="cnpj">CNPJ: </label>
        <input class="form-control input-lg" id="cnpj" type="text" name="cnpj" value="{{$reg->cnpj or old('cnpj')}}">
    </div>

    <div class="col-sm-6">
        <label for="razao_social">Razão Social: </label>
        <input class="form-control input-lg" id="razao_social" type="text" name="razao_social" value="{{$reg->razao_social or old('razao_social')}}">
    </div>

    <div class="col-sm-10">
        <button type="submit" class="btn btn-danger">Salvar Empresa</button>
    </div>
</form>

@endsection