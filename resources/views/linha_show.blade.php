@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2>Cadastro de Linhas</h2>
</div>

<div class="col-sm-6">

    <div class='col-sm-3'>
        <a href="{{route('linhas.create')}}" class="btn btn-danger" role="button">Nova Linha</a>

    </div>

 

</div>




<div class='col-sm-12'>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>Cód</th>
                <th>Cod_Linha</th>
                <th>Nome_Linha</th>
                <th>Modalidade</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>

            @foreach($linhas as $linha)

            <tr>
                <td>{{$linha->id}}</td>
                <td>{{$linha->cod_linha}}</td>
                <td>{{$linha->nome_linha}}</td>
                <td>{{$linha->modalidade}}</td>


                <td>
                    <a href="{{route('linhas.edit', $linha->id)}}" class="btn btn-success" role="button">Editar</a>
                    <form style="display: inline-block;" method="POST" action="{{route('linhas.destroy', $linha->id)}}"
                          onsubmit="return confirm('Confirmar Exclusão ?')">
                        {{method_field('DELETE')}}
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-danger">Excluir</button>


                    </form>



                </td>

            </tr>

            @endforeach
        </tbody>
    </table>
</div>



@endsection

