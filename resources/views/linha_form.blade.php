@extends('principal')

@section('conteudo')

@if ($acao == 1)
<form method="post" action="{{route('linhas.store')}}">
    @else
    <form method="post" action="{{route('linhas.update', $reg->id)}}">
        {!! method_field('put') !!}
        @endif
        {{ csrf_field() }}

       
        
        <div class="col-sm-4">
            <label for="cod_linha">Cód Linha: </label>
            <input class="form-control input-lg" id="cod_linha" type="text" name="cod_linha" value="{{$reg->cod_linha or old('cod_linha')}}">
        </div>

        <div class="col-sm-6">
            <label for="nome_linha">Nome de Linha: </label>
            <input class="form-control input-lg" id="nome_linha" type="text" name="nome_linha" value="{{$reg->nome_linha or old('nome_linha')}}">
        </div>

        <div class="col-sm-2">
            <label for="modalidade">Modalidade:</label>
            <select class="form-control" id="combustivel" name="modalidade">
                <option value="CM"
                        @if ((isset($reg) and $reg->modalidade == "Comum") or 
                        old('modalidade') == "CM") selected @endif                        
                        >Comum</option>
               <option value="DI"
                        @if ((isset($reg) and $reg->modalidade == "Direto") or 
                        old('modalidade') == "DI") selected @endif                        
                        >Direto</option>
               <option value="SD"
                        @if ((isset($reg) and $reg->modalidade == "SemiDireto") or 
                        old('modalidade') == "SD") selected @endif                        
                        >SemiDireto</option>
                 <option value="IO"
                        @if ((isset($reg) and $reg->modalidade == "Integração ônibus") or 
                        old('modalidade') == "IO") selected @endif                        
                        >Integração ônibus</option>
                 <option value="RT"
                        @if ((isset($reg) and $reg->modalidade == "Rota") or 
                        old('modalidade') == "RT") selected @endif                        
                        >Rota</option>
                 <option value="IT"
                        @if ((isset($reg) and $reg->modalidade == "Integração Trem") or 
                        old('modalidade') == "IT") selected @endif                        
                        >Integração Trem</option>
                 <option value="ST"
                        @if ((isset($reg) and $reg->modalidade == "Seletivo") or 
                        old('modalidade') == "ST") selected @endif                        
                        >Seletivo</option>
                 <option value="EX"
                        @if ((isset($reg) and $reg->modalidade == "Executivo") or 
                        old('modalidade') == "EX") selected @endif                        
                        >Executivo</option>
                 
                 
                 
            </select>
        </div>

        <div class="col-sm-10">
            <button type="submit" class="btn btn-danger">Salvar Empresa</button>
        </div>
    </form>

    @endsection