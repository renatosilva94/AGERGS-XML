@extends('principal')

@section('conteudo')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<textarea class="form-control" style="height:600px; width:1000px" id="download">
<?php echo'<?xml version="1.0" encoding="UTF-8"?>'?>

<carga_dados>
    @foreach ($empresas as $empresa)   
    @foreach($indicadores as $indicador)
    <cnpj>{{$empresa->cnpj}}</cnpj>
    <razao_social>{{$empresa->razao_social}}</razao_social>
    <mes_ano>{{$indicador->mes_ano}}</mes_ano>
    @endforeach
    @endforeach
    <carga_linha>
        @foreach($linhas as $linha)
        <cod_linha>{{$linha->cod_linha}}</cod_linha>
        <nome_linha>{{$linha->nome_linha}}</nome_linha>
        <modalidade>{{$linha->modalidade}}</modalidade>
        @endforeach
        <carga_indicador>
            @foreach($indicadores as $indicador)
            <id_indicador>301</id_indicador>
            <cod_precisao>{{$indicador->icpoPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->icpo}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>302</id_indicador>
            <cod_precisao>{{$indicador->viagensrealizadasPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->viagens_realizadas}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>303</id_indicador>
            <cod_precisao>{{$indicador->viagensprogramadasPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->viagens_programadas}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>304</id_indicador>
            <cod_precisao>{{$indicador->iphsPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->iphs}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>305</id_indicador>
            <cod_precisao>{{$indicador->viagensimpontuaisPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->viagens_impontuais}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>306</id_indicador>
            <cod_precisao>{{$indicador->icviPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->icvi}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>307</id_indicador>
            <cod_precisao>{{$indicador->viagensinterrompidasPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->viagens_interrompidas}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>308</id_indicador>
            <cod_precisao>{{$indicador->imfuPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->imfu}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>309</id_indicador>
            <cod_precisao>{{$indicador->ioqPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->ioq}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>310</id_indicador>
            <cod_precisao>{{$indicador->idiPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->idi}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>311</id_indicador>
            <cod_precisao>{{$indicador->io1Precisao}}</cod_precisao>
            <val_indicador>{{$indicador->io1}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>312</id_indicador>
            <cod_precisao>{{$indicador->io2Precisao}}</cod_precisao>
            <val_indicador>{{$indicador->io2}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>313</id_indicador>
            <cod_precisao>{{$indicador->io3Precisao}}</cod_precisao>
            <val_indicador>{{$indicador->io3}}</val_indicador>
        </carga_indicador>
        <carga_indicador>
            <id_indicador>314</id_indicador>
            <cod_precisao>{{$indicador->ioapPrecisao}}</cod_precisao>
            <val_indicador>{{$indicador->ioap}}</val_indicador>
            @endforeach
        </carga_indicador>
    </carga_linha>
</carga_dados>
</textarea>

<button class="btn btn-success">Efetuar Download</button>

<script>
$("button").click(function () {
// create `a` element
$("<a />", {
// if supported , set name of file
//download: $.now() + ".txt",
download: {{$empresa -> cnpj}} + "-ITM-" + {{$indicador->ano_mes}} + ".xml",
        // set `href` to `objectURL` of `Blob` of `textarea` value
        href: URL.createObjectURL(
                new Blob([$("textarea").val()], {
                type: "text/plain"
                }))
        })
        // append `a` element to `body`
        // call `click` on `DOM` element `a`
        .appendTo("body")[0].click();
// remove appended `a` element after "Save File" dialog,
// `window` regains `focus` 
$(window).one("focus", function () {
$("a").last().remove()
        })
})</script>

@endsection