@extends('principal')

@section('conteudo')

<script>
    $(document).ready(function () {
        var viagens_realizadas = document.getElementById('viagens_realizadas');
        var viagens_programadas = document.getElementById('viagens_programadas');
        var viagens_impontuais = document.getElementById('viagens_impontuais');
        var viagens_interrompidas = document.getElementById('viagens_interrompidas');
        var ocorrencia_quebra = document.getElementById('ocorrencia_quebra');
        var desvio_itinerario = document.getElementById('desvio_itinerario');
        var viagens_abaixo_80 = document.getElementById('viagens_abaixo_80');
        var viagens_acima_80 = document.getElementById('viagens_acima_80');
        var viagens_100 = document.getElementById('viagens_100');
        var acidentes_transito = document.getElementById('acidentes_transito');



        //Calculo do ICPO
        $('#viagens_programadas').on('focusout', function () {
            var icpoResultado = parseFloat(viagens_realizadas.value) / parseFloat(viagens_programadas.value);
            icpo.value = icpoResultado;
        });

        //Calculo do IPHS
        $('#viagens_impontuais').on('focusout', function () {
            var iphsResultado = (parseFloat(viagens_realizadas.value) - parseFloat(viagens_impontuais.value)) / parseFloat(viagens_realizadas.value);
            iphs.value = iphsResultado;
        });

        //Calculo do ICVI
        $('#viagens_interrompidas').on('focusout', function () {
            var icviResultado = (parseFloat(viagens_realizadas.value) - parseFloat(viagens_interrompidas.value)) / parseFloat(viagens_realizadas.value);
            icvi.value = icviResultado;
        });

        //Calculo do IMFU

        //Indice do IOQ
        $('#ocorrencia_quebra').on('focusout', function () {
            var ioqResultado = ((parseFloat(ocorrencia_quebra.value) / parseFloat(viagens_realizadas.value))) * 1000;
            ioq.value = ioqResultado;
        });

        //Calculo do IDI
        $('#desvio_itinerario').on('focusout', function () {
            var idiResultado = ((parseFloat(desvio_itinerario.value) / parseFloat(viagens_realizadas.value))) * 1000;
            idi.value = idiResultado;
        });

        //Calculo do IO1
        $('#viagens_abaixo_80').on('focusout', function () {
            var io1Resultado = ((parseFloat(viagens_abaixo_80.value) / parseFloat(viagens_realizadas.value))) * (100);
            io1.value = io1Resultado;
        });

        //Calculo do IO2
        $('#viagens_acima_80').on('focusout', function () {
            var io2Resultado = ((parseFloat(viagens_acima_80.value) / parseFloat(viagens_realizadas.value))) * (100);
            io2.value = io2Resultado;
        });

        //Calculo do IO3
        $('#viagens_100').on('focusout', function () {
            var io3Resultado = ((parseFloat(viagens_100.value) / parseFloat(viagens_realizadas.value))) * (100);
            io3.value = io3Resultado;
        });

        //Calculo do IOAP
        $('#acidentes_transito').on('focusout', function () {
            var ioapResultado = ((parseFloat(acidentes_transito.value) / parseFloat(viagens_realizadas.value))) * 1000;
            ioap.value = ioapResultado;
        });





    });

</script>
@if($acao == 1)
<form method="post" action="{{route('indicadores.store')}}">
    @else
    <form method="post" action="{{route('indicadores.update', $reg->id)}}">
        {!! method_field('put') !!}
        @endif
        {{ csrf_field() }}

        <div class="col-sm-12">
            <label for="ano_mes"> Ano e Mês (AAAAMM) : </label>
            <input type="number" class="form-control input-lg" id="ano_mes" name="ano_mes" step="any" placeholder="AAAAMM">
        </div>

        <div class="col-sm-12">
            <label for="mes_ano">Ano, Mês e Primeiro Dia (AAAA-MM-01) : </label>
            <input type="text" class="form-control input-lg" id="mes_ano" name="mes_ano" placeholder="AAAA-MM-01">
        </div>

        <div class="col-sm-6">
            <label for="viagens_realizadas">Viagens Realizadas: </label>
            <input class="form-control input-lg" id="viagens_realizadas" type="number" name="viagens_realizadas" value="{{$reg->viagens_realizadas or old('viagens_realizadas')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagensrealizadasPrecisao">Viagens Realizadas Precisão: </label>
            <select class="form-control input-lg" id="viagensrealizadasPrecisao" name="viagensrealizadasPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>



        <div class="col-sm-6">
            <label for="viagens_programadas">Viagens Programadas: </label>
            <input class="form-control input-lg" id="viagens_programadas" type="number" name="viagens_programadas" value="{{$reg->viagens_programadas or old('viagens_programadas')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagensprogramadasPrecisao">Viagens Programadas Precisão: </label>
            <select class="form-control input-lg" id="viagensprogramadasPrecisao" name="viagensprogramadasPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="viagens_impontuais">Viagens Impontuais: </label>
            <input class="form-control input-lg" id="viagens_impontuais" type="number" name="viagens_impontuais" value="{{$reg->viagens_impontuais or old('viagens_impontuais')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagensimpontuaisPrecisao">Viagens Impontuais Precisão: </label>
            <select class="form-control input-lg" id="viagensimpontuaisPrecisao" name="viagensimpontuaisPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="viagens_interrompidas">Viagens Interrompidas: </label>
            <input class="form-control input-lg" id="viagens_interrompidas" type="number" name="viagens_interrompidas" value="{{$reg->viagens_interrompidas or old('viagens_interrompidas')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagensinterrompidasPrecisao">Viagens Interrompidas Precisão: </label>
            <select class="form-control input-lg" id="viagensinterrompidasPrecisao" name="viagensinterrompidasPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>


        <div class="col-sm-6">
            <label for="ocorrencia_quebra">Ocorrência de Quebra: </label>
            <input class="form-control input-lg" id="ocorrencia_quebra" type="number" name="ocorrencia_quebra" value="{{$reg->ocorrencia_quebra or old('ocorrencia_quebra')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="desvio_itinerario">Desvios de Itinerario: </label>
            <input class="form-control input-lg" id="desvio_itinerario" type="number" name="desvio_itinerario" value="{{$reg->desvio_itinerario or old('desvio_itinerario')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagens_abaixo_80">Viagens Abaixo de 80%: </label>
            <input class="form-control input-lg" id="viagens_abaixo_80" type="number" name="viagens_abaixo_80" value="{{$reg->viagens_abaixo_80 or old('viagens_abaixo_80')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagens_acima_80">Viagens Acima de 80%: </label>
            <input class="form-control input-lg" id="viagens_acima_80" type="number" name="viagens_acima_80" value="{{$reg->viagens_acima_80 or old('viagens_acima_80')}}" step="any">
        </div>

        <div class="col-sm-6">
            <label for="viagens_100">Viagens 100%: </label>
            <input class="form-control input-lg" id="viagens_100" type="number" name="viagens_100" value="{{$reg->viagens_100 or old('viagens_100')}}" step="any">
        </div>



        <div class="col-sm-6">
            <label for="acidentes_transito">Acidente de Transito: </label>
            <input class="form-control input-lg" id="acidentes_transito" type="number" name="acidentes_transito" value="{{$reg->acidentes_transito or old('acidentes_transito')}}" step="any"> 
        </div>

        <!-- Calculos  -->


        <div class="col-sm-6">
            <label for="icpo">ICPO : </label>
            <input type="number" class="form-control input-lg" id="icpo" name="icpo" value="icpo" step="any">
        </div>

        <div class="col-sm-6">
            <label for="icpoPrecisao">ICPO Precisão: </label>
            <select class="form-control input-lg" id="icpoPrecisao" name="icpoPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>



        <div class="col-sm-6">
            <label for="iphs">IPHS : </label>
            <input type="number" class="form-control input-lg" id="iphs" name="iphs" value="iphs" step="any">
        </div>

        <div class="col-sm-6">
            <label for="iphsPrecisao">IPHS Precisão: </label>
            <select class="form-control input-lg" id="iphsPrecisao" name="iphsPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>



        <div class="col-sm-6">
            <label for="icvi">ICVI : </label>
            <input type="number" class="form-control input-lg" id="icvi" name="icvi" value="icvi" step="any">
        </div>

        <div class="col-sm-6">
            <label for="icviPrecisao">ICVI Precisão: </label>
            <select class="form-control input-lg" id="icviPrecisao" name="icviPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>


        <div class="col-sm-6">
            <label for="imfu">IMFU : </label>
            <input class="form-control input-lg" id="imfu" type="number" name="imfu" value="imfu" step="any">
        </div>

        <div class="col-sm-6">
            <label for="imfuPrecisao">IMFU Precisão: </label>
            <select class="form-control input-lg" id="imfuPrecisao" name="imfuPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>


        <div class="col-sm-6">
            <label for="ioq">IOQ : </label>
            <input type="number" class="form-control input-lg" id="ioq" name="ioq" value="ioq" step="any">
        </div>

        <div class="col-sm-6">
            <label for="ioqPrecisao">IOQ Precisão: </label>
            <select class="form-control input-lg" id="ioqPrecisao" name="ioqPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="idi">IDI : </label>
            <input type="number" class="form-control input-lg" id="idi" name="idi" value="idi" step="any">
        </div>

        <div class="col-sm-6">
            <label for="idiPrecisao">IDI Precisão: </label>
            <select class="form-control input-lg" id="idiPrecisao" name="idiPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="io1">IO1 : </label>
            <input type="number" class="form-control input-lg" id="io1" name="io1" value="io1" step="any">
        </div>

        <div class="col-sm-6">
            <label for="io1Precisao">IO1 Precisão: </label>
            <select class="form-control input-lg" id="io1Precisao" name="io1Precisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="io2">IO2 : </label>
            <input type="number" class="form-control input-lg" id="io2" name="io2" value="io2" step="any">
        </div>

        <div class="col-sm-6">
            <label for="io2Precisao">IO2 Precisão: </label>
            <select class="form-control input-lg" id="io2Precisao" name="io2Precisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="io3">IO3 : </label>
            <input type="number" class="form-control input-lg" id="io3" name="io3" value="io3" step="any">
        </div>

        <div class="col-sm-6">
            <label for="io3Precisao">IO3 Precisão: </label>
            <select class="form-control input-lg" id="io3Precisao" name="io3Precisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>

        <div class="col-sm-6">
            <label for="ioap">IOAP : </label>
            <input type="number" class="form-control input-lg" id="ioap" name="ioap" value="ioap" step="any">
        </div>

        <div class="col-sm-6">
            <label for="ioapPrecisao">IOAP Precisão: </label>
            <select class="form-control input-lg" id="ioapPrecisao" name="ioapPrecisao">
                <option>A</option>
                <option>B</option>
                <option>C</option>
            </select>
        </div>


        <div class="col-sm-10">
            <button type="submit" class="btn btn-danger">Salvar Indicador</button>
        </div>



    </form>

    @endsection